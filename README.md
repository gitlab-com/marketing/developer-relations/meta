# Developer Relations Team Meta Project

This project contains resources and work items related to cross-functional collaboration on GitLab's [Developer Relations](https://handbook.gitlab.com/handbook/marketing/developer-relations/) team. The following sub-teams comprise Developer Relations:

* [Contributor Success](https://handbook.gitlab.com/handbook/marketing/developer-relations/contributor-success/)
* [Developer Relations](https://handbook.gitlab.com/handbook/marketing/developer-relations/developer-evangelism/) and [Technical Marketing](https://handbook.gitlab.com/handbook/marketing/developer-relations/technical-marketing/)
* [Community Programs](https://handbook.gitlab.com/handbook/marketing/developer-relations/community-programs/)